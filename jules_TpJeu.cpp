#include <iostream>
#include <ctime>
#include <stdlib.h>
#include <time.h>

//https://careerkarma.com/blog/c-plus-plus-string-to-int/#:~:text=In%20C%2B%2B%20the%20stoi(),distinguish%20particular%20types%20of%20data.

using namespace std;

int main()
{
    srand(time(NULL));
    int nombreAleatoire= rand() % 101;
    int nombreTentatives = 0;
    bool valeurTrouvee = false;

    cout << "Veuillez saisir un nombre compris entre 0 et 100" << endl;

    while (!valeurTrouvee)
    {
        string saisie;
        cin >> saisie;

        int valeurSaisie;

        if (valeurSaisie == nombreAleatoire)
        {
            valeurTrouvee = true;
        }
        else
        {
            if (valeurSaisie < nombreAleatoire)
            {
                cout << "c'est plus ..." << endl;
            }
            else
            {
                cout << "C'est moins ..." << endl;
            }
        }
        nombreTentatives++;
    }
    cout << "Trouve en " << nombreTentatives << " tentative(s)" << endl;
}
